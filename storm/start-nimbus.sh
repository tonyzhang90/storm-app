#!/bin/bash
rm -rf ~/zookeeper/*
rm -rf ~/storm-local/*
rm -rf logs/*
numactl  --cpunodebind=0  --membind=0 ~/Documents/zookeeper-3.5.1-alpha/bin/zkServer.sh start
numactl  --cpunodebind=0  --membind=0 ~/Documents/apache-storm-1.1.0/bin/storm nimbus
