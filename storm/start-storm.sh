#!/bin/bash
rm -fr ~/storm-local/*
rm ~/Documents/apache-storm-1.1.0/logs/*
numactl  --cpunodebind=0  --membind=0 ~/Documents/zookeeper-3.5.1-alpha/bin/zkServer.sh start&
numactl  --cpunodebind=0  --membind=0 ~/Documents/apache-storm-1.1.0/bin/storm nimbus&
numactl  --cpunodebind=0  --membind=0 ~/Documents/apache-storm-1.1.0/bin/storm ui&
